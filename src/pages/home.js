import React from "react";
import { JumbotronContainer } from "../containers/Jumbotron";
import { FaqsContainer } from "../containers/accordion";
import { FooterContainer } from "../containers/footer";
import { HeaderContainer } from "../containers/header";
import { Feature, OptForm } from "../components";

export default function Home() {
  return (
    <>
      <HeaderContainer>
        <Feature>
          <Feature.Title>
            100% entertainment. Pay for 1 month, get 1 month free.
          </Feature.Title>
          <Feature.SubTitle>Watch anywhere. Cancel anytime.</Feature.SubTitle>

          <OptForm>
            <OptForm.Text>
              Ready to watch? Enter your email to create or restart your
              membership.
            </OptForm.Text>
            <OptForm.Break />
            <OptForm.Input placeholder="Email address" />
            <OptForm.Button>GET 1 MONTH FREE</OptForm.Button>
            <OptForm.Break />
            <OptForm.Text>
              Only new members are eligible for this offer.
            </OptForm.Text>
          </OptForm>
        </Feature>
      </HeaderContainer>

      <JumbotronContainer />
      <FaqsContainer />
      <FooterContainer />
    </>
  );
}
